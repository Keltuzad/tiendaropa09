package com.tiendaropa09.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="productos")
public class Producto {
    
   @Id
   @Column(name="id_productos")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id_productos;
   
   @Column(name="codigo", length=10, nullable=false, unique=true )
   private String codigo;
   
   @Column(name="nombre", length=45, nullable=false)
   private String nombre;
   
    @Column(name="talla", length=10, nullable=false)
   private String talla;
   
    @Column(name="imagen", length=100, nullable=false )
   private String imagen;
    
   @Column(name="disponibilidad",nullable = false, columnDefinition = "TINYINT")
   private Boolean disponibilidad;
    
    @Column(name="valor_proveedor" )
   private Float valor_proveedor;
    
     @Column(name="valor_venta" )
   private Float valor_venta;
     
     @Column(name="lucro" )
   private Float lucro;

    @ManyToOne
    @JoinColumn(name = "id_categorias")
    private Categorias categoria;   

    public Producto(int id_productos, String codigo, String nombre, String talla, String imagen, Boolean disponibilidad, Float valor_proveedor, Float valor_venta, Float lucro, Categorias categoria) {
        this.id_productos = id_productos;
        this.codigo = codigo;
        this.nombre = nombre;
        this.talla = talla;
        this.imagen = imagen;
        this.disponibilidad = disponibilidad;
        this.valor_proveedor = valor_proveedor;
        this.valor_venta = valor_venta;
        this.lucro = lucro;
        this.categoria = categoria;
    }

    public Producto() {
    }

    public Producto(String codigo, String nombre, String talla, String imagen, Boolean disponibilidad) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.talla = talla;
        this.imagen = imagen;
        this.disponibilidad = disponibilidad;
    }

    public Producto(String codigo, String nombre, String talla, String imagen, Boolean disponibilidad, Float valor_proveedor, Float valor_venta, Float lucro, Categorias categoria) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.talla = talla;
        this.imagen = imagen;
        this.disponibilidad = disponibilidad;
        this.valor_proveedor = valor_proveedor;
        this.valor_venta = valor_venta;
        this.lucro = lucro;
        this.categoria = categoria;
    }

    public int getId_productos() {
        return id_productos;
    }

    public void setId_productos(int id_productos) {
        this.id_productos = id_productos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Float getValor_proveedor() {
        return valor_proveedor;
    }

    public void setValor_proveedor(Float valor_proveedor) {
        this.valor_proveedor = valor_proveedor;
    }

    public Float getValor_venta() {
        return valor_venta;
    }

    public void setValor_venta(Float valor_venta) {
        this.valor_venta = valor_venta;
    }

    public Float getLucro() {
        return lucro;
    }

    public void setLucro(Float lucro) {
        this.lucro = lucro;
    }

    public Categorias getCategoria() {
        return categoria;
    }

    public void setCategoria(Categorias categoria) {
        this.categoria = categoria;
    }
    
}
