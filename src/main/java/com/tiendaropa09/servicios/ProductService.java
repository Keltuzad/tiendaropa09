package com.tiendaropa09.servicios;

import com.tiendaropa09.entidades.Producto;
import com.tiendaropa09.repositorios.IProdRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
      
    @Autowired
    private IProdRepository repo;
    
    public Producto crearNuevoProducto(Producto p){
        Producto guardado = repo.save(p);
        System.out.println(guardado);
        return guardado;
    }
    public Producto actualizarProducto(Producto p){
        Producto guardado = repo.save(p);
        return guardado;
    }
    public Producto consultarProducto(int id){
        Producto p = repo.findById(id).get();
        return p;
    }
    public Producto consultarProductoOrNull(int id) {
        Producto p = repo.findById(id).orElse(null);
        return p;
    }
    public List<Producto> consultarProducto(String nombre){
        List<Producto> lista = repo.findByNombreContaining(nombre);
        return lista;
    }
    
//    public List<Productos> consultarProducto(Integer id_categorias){
//        List<Productos> lista = repo.findByid_categoriasContaining(id_categorias);
//        return lista;
//    }
    
    
    
    public List<Producto> consultarProductos(String criterio) {
        List<Producto> lista = repo.findByNombreContainingOrCodigoContaining(criterio, criterio);
        return lista;
    }
    
    public List<Producto> consultarProductos() {
        List<Producto> lista = repo.findAll();
        return lista;
    }
    public void eliminarProducto(Producto p){
       repo.delete(p);  
    }  
}
