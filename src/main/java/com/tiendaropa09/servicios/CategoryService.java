package com.tiendaropa09.servicios;

import com.tiendaropa09.entidades.Categorias;
import com.tiendaropa09.repositorios.ICatRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    
    @Autowired
    private ICatRepository repo;
    
    public Categorias crearNuevaCategoria(Categorias c){
        Categorias guardado = repo.save(c);
        System.out.println(guardado);
        return guardado;
    }
    public Categorias actualizarCategoria(Categorias c){
        Categorias guardado = repo.save(c);
        return guardado;
    }
    public Categorias consultarCategoria(int id){
        Categorias g = repo.findById(id).get();
        return g;
    }
    public Categorias consultarCategoriaOrNull(int id) {
        Categorias g = repo.findById(id).orElse(null);
        return g;
    }
    public List<Categorias> consultarCategoria(String nombre){
        List<Categorias> lista = repo.findByNombreContaining(nombre);
        return lista;
    }
    
    public List<Categorias> consultarCategorias(String criterio) {
        List<Categorias> lista = repo.findByNombreContainingOrDescripcionContaining(criterio, criterio);
        return lista;
    }
    
    public List<Categorias> consultarCategorias() {
        List<Categorias> lista = repo.findAll();
        return lista;
    }
    public void eliminarCategoria(Categorias g){
       repo.delete(g);  
    }
}
