package com.tiendaropa09.repositorios;

import com.tiendaropa09.entidades.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProdRepository extends JpaRepository<Producto, Integer>{
    
   // public void findByCode(String codigo);
    public List<Producto> findByNombreContaining(String nombre);
//    public List<Productos> findByid_categoriasContaining(Integer id_categorias);
    public List<Producto> findByNombreContainingOrCodigoContaining(String nombre, String codigo);
    
}   
