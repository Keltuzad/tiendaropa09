package com.tiendaropa09.repositorios;

import com.tiendaropa09.entidades.Categorias;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICatRepository extends JpaRepository<Categorias, Integer>{
    
   // public void findByCode(String codigo);
    public List<Categorias> findByNombreContaining(String nombre);
    public List<Categorias> findByNombreContainingOrDescripcionContaining(String nombre, String descripcion);
    
}
