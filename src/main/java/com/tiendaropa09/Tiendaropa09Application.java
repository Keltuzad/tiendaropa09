package com.tiendaropa09;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tiendaropa09Application {

	public static void main(String[] args) {
		SpringApplication.run(Tiendaropa09Application.class, args);
	}

}
