package com.tiendaropa09.controladores;

import com.tiendaropa09.servicios.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * index.html             /
 * listacategorias.html   /generos
 * formcategories.html    /categories/form/{id}
 * @author caman
 */
@Controller
public class CategoriesController {
    
    @Autowired
    private CategoryService servicio;
     
    @GetMapping("/")
    public String cargarIndex() {
        return "index";
}   
    @GetMapping("/listcategories")
    public String cargarListCategories(Model model,@RequestParam(value="criterio", required=false) String criterio) {
        if(criterio == null){
            model.addAttribute("categorias", servicio.consultarCategorias());
        }
        else{
            model.addAttribute("categorias", servicio.consultarCategorias(criterio));
            model.addAttribute("criterio",criterio);
        }
        return "listcategories";      
}   
    @GetMapping("/formcategories")
    public String cargarFormCategories() {
        return "formCategories";
}
    @GetMapping("/dama")
    public String cargarDama() {
        return "dama";
}        
    @GetMapping("/login")
    public String cargarLogin() {
        return "login";
}  
    @GetMapping("/registro")
    public String cargarRegistro() {
        return "registro";
}

}
