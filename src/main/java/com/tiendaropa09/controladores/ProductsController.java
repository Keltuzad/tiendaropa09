package com.tiendaropa09.controladores;

import com.tiendaropa09.entidades.Producto;
import com.tiendaropa09.servicios.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductsController {
    
    @Autowired
    private ProductService serv;
           
//     @GetMapping("/listproductos")
//    public String cargarProductos(Model model) {
//        List<Producto> productos = serv.consultarProductos();
//        model.addAttribute("productos", productos);
//        return "listaProductos";

    @GetMapping("/listproductos")
    public String cargarListProducts(Model model,@RequestParam(value="criterio", required=false) String criterio) {
        List<Producto> productos; 
        if(criterio == null){
            productos = serv.consultarProductos();
        }
        else{
            productos = serv.consultarProductos(criterio);            
        }
        model.addAttribute("productos", productos);
        return "listaProductos";      
    }   
}
