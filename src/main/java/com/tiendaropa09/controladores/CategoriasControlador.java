package com.tiendaropa09.controladores;

import com.tiendaropa09.entidades.Categorias;
import com.tiendaropa09.servicios.CategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoriasControlador {
   
    @Autowired
    private CategoryService servicio;
    
    @GetMapping("/api/categorias")
    public ResponseEntity<List<Categorias>> consultarCategorias(){
        List<Categorias> lista = servicio.consultarCategorias();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }
    
}
