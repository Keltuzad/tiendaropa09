package com.tiendaropa09;

import com.tiendaropa09.entidades.Categorias;
import com.tiendaropa09.entidades.Producto;
import com.tiendaropa09.servicios.CategoryService;
import com.tiendaropa09.servicios.ProductService;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProductosUnitTest {
    
    @Autowired
    private CategoryService categoriaServicio;
    
    @Autowired
    private ProductService productoServicio;
    
    @Test
    @Disabled("Registro ya Creado")
    public void verificarSiSeGuardaUnProductoNuevo(){
    Categorias c = categoriaServicio.consultarCategoria(1);
    Producto p = new Producto("105725", "Pantalon Caballero", "32", "https://patprimo.vteximg.com.br/arquivos/ids/1045474-1500-1800/pantalones-para-hombre-44070944-50674_5.jpg?v=637648509137670000", true, 50000f , 80000f , 30000f , c);
    Assertions.assertDoesNotThrow(() -> {
           productoServicio.crearNuevoProducto(p);
        }, "Error al guardar Producto");
    }
    
    @Test
    public void verificarSiCarganProductos(){
        List<Producto> productos = productoServicio.consultarProductos();
        Assertions.assertTrue( productos.size() > 0, "Error, no se pudo cargar Canciones");
    }
    
    @Test
    public void verificarSiCarganProductosporCriterio(){
        List<Producto> productos = productoServicio.consultarProductos("jean");
        Assertions.assertTrue( productos.size() > 0, "Error, no se pudo cargar Canciones");
         List<Producto> productosPorCodigo = productoServicio.consultarProductos("SA");
         Assertions.assertTrue( productos.size() > 0, "Error, no se pudo cargar Canciones");
    }
}
