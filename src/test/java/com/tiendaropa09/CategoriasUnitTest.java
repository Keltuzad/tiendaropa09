package com.tiendaropa09;

import com.tiendaropa09.entidades.Categorias;
import com.tiendaropa09.servicios.CategoryService;
import com.tiendaropa09.entidades.Producto;
import com.tiendaropa09.servicios.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

@SpringBootTest
class CategoriasUnitTest {

    @Autowired
    private CategoryService servicio;
    
	@Test
        @Disabled("Registro ya Creado")
	void verificarSiSeGuardaUnaNuevaCategoria() {
            Categorias c = new Categorias("Jeans","Toda tipo de Jeans de diferentes tallas");
            Categorias guardado = servicio.crearNuevaCategoria(c);
            System.out.println(guardado);
            Assertions.assertTrue( guardado.getId_categoria() > 0, "Error, no se pudo guardar nuevo Genero");
        }
        @Test
        @Disabled("Registro ya Creado")
	void verificarUnaNuevaCategoria() {
            Categorias c = new Categorias("Jeans","Toda tipo de Jeans de diferentes tallas");
            Assertions.assertDoesNotThrow(()-> {
                servicio.crearNuevaCategoria(c); 
            },  "Error, no se pudo guardar nuevo Genero");
        }
        
        @Test
        @Disabled("Registro ya Creado")
        void verificarSiSeCarganTodasLasCategoria() {
        Categorias c = new Categorias(null, null);
        Assertions.assertThrows(DataIntegrityViolationException.class,() -> {
            servicio.crearNuevaCategoria(c);
        }, "No se genero Exception desde la BD");
        }
        
//        @Test
//        void verificarSiSeCarganTodosLosGeneros(){
//            List<Categorias> lista = servicio.consultarCategoria
//        }
}

